The files in repository contains the experiments' raw data for the work
presented in "[Thermal design power and vectorized instructions
behavior](https://hal.archives-ouvertes.fr/hal-02167083v2)".

The experiments for this work were run on three platforms from [Grid'5000](https://www.grid5000.fr/w/Hardware) testbed:
- nova: Intel(R) Xeon(R) CPU E5-2620 v4 (Broadwell architecture with SSE, AVX and AVX2 instruction sets)
- chifflet: Intel Xeon E5-2680 v4 (Broadwell architecture with SSE, AVX and AVX2 instruction sets)
- yeti: Intel(R) Xeon(R) Gold 6130 (Skylake architecture with SSE, AVX, AVX2 and AVX512 instruction sets).

The files are organized in three directory, one per platform. Each
file is named according to the configuration of the run. It has the
following format : `appli_power-{inst}` where `inst` can be SSE, AVX,
AVX2, noTB-SSE, noTB-AVX and noTB-AVX2 for all three platforms. AVX512
and noTB-AVX512 are available for yeti only. Note that noTB stands for
"no turboboost".
Power measurements rely on [LIKWID](https://github.com/RRZE-HPC/likwid) (version 4.3.0).

The header of each file describes the presented data, and each line provides the data for one application.
Four applications are used:
- [HPL](http://icl.utk.edu/hpl/): High Performance Linpack benchmark that solves dense linear algebra systems
- [PLASMA SVD](https://bitbucket.org/icl/plasma/src/main/): Singular Value Decomposition that computes the singular values of a matrix
- [SToRM](https://github.com/laurentnoe/storm): Seed-based read mapping tool for SOLiD or Illumina sequencing genome
- [AFF3CT](https://aff3ct.github.io/): A Fast Forward Error Correction Toolbox 
